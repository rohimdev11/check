var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/heath-check', (req, res, next) =>{
  res.json({
    message:'Health check success'
  })
})

router.get('/hello', (req, res, next) =>{
  res.json({
    message:`Hello ${process.env.APP_NAME}`
  })
})

module.exports = router;
